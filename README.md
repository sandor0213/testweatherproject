Code style - The Official raywenderlich.com Swift Style Guide.
https://github.com/raywenderlich/swift-style-guide#function-declarations
Except: 
1. Long lines should be wrapped at around 70 characters. A hard limit is intentionally not specified.
2. Indent using 2 spaces rather than tabs to conserve space and help prevent line wrapping. (In use 4 spaces)
3. If the call site must be wrapped, put each parameter on a new line, indented one additional level And Similar
