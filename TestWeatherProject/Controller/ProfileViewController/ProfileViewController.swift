//
//  ProfileViewController.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var logOutButton: UIButton!
    
    private weak var profileViewModel: ProfileViewModel! {
        didSet {
            avatarImageView.image = profileViewModel.avatarImage
            nameLabel.text = profileViewModel.name
        }
    }
    
    weak var delegateMain: MainFlowCoordinatorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    @IBAction func logOutButtonAction(_ sender: Any) {
        AuthenticationManager.logOutFacebook()
        StorageManager.shared.clearLocalDatabase(forEntitiesName: [StorageConfiguration.profilesEntityName, StorageConfiguration.locationsEntityName])
        delegateMain?.navigateToAuthFlow()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        delegateMain?.navigateToLocationViewController()
    }
    
    fileprivate func setUpView() {
        avatarImageView.setCircle()
        logOutButton.setNegativeButton()
        requestProfileViewModel()
    }
    
    private func requestProfileViewModel() {
        ProfileViewModel.getProfileViewModel(completion: { [weak self] (profileViewModel) in
            self?.profileViewModel = profileViewModel
        })
    }
}
