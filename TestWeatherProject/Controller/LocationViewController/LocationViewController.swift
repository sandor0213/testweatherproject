//
//  LocationViewController.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

final class LocationViewController: UIViewController {
    
    @IBOutlet weak var locationTableView: UITableView!
    
    weak var delegate: MainFlowCoordinatorDelegate?
    
    private var locationViewModels: [LocationViewModel] = [LocationViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        requestLocationViewModel()
    }
    
    @IBAction func addLocationButtonAction(_ sender: Any) {
        delegate?.navigateToSearchViewController()
    }
    
    @IBAction func profileButtonAction(_ sender: Any) {
        delegate?.navigateToProfileViewController()
    }
    
    private func requestLocationViewModel() {
        LocationViewModel.getLocationViewModels { [weak self] (locationViewModels) in
            self?.locationViewModels = locationViewModels
            self?.locationTableView.reloadData()
        }
    }
}

extension LocationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = locationViewModels[indexPath.row].name
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if let locationName = locationViewModels[indexPath.row].name {
                LocationViewModel.deleteLocationViewModel(entityKeyValue: locationName)
                requestLocationViewModel()
                tableView.reloadData()
            }
        }
    }
}

extension LocationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.navigateToForecastViewController(locationViewModel: locationViewModels[indexPath.row])
    }
}
