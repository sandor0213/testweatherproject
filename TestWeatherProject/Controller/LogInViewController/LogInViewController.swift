//
//  LogInViewController.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

final class LogInViewController: UIViewController {
    
    weak var delegate: AuthFlowCoordinatorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginViaFbTapped(_ sender: Any) {
        AuthenticationManager.loginViaFacebook(controller: self) { [weak self] (error) in
            if error == nil {
                self?.delegate?.navigateToMainFlow()
            }
        }
    }
}
