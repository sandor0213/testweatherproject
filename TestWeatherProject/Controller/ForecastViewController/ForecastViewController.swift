//
//  ForecastViewController.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/10/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

final class ForecastViewController: UIViewController {
    
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var apparentTemperatureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var precipProbabilityLabel: UILabel!
    
    var locationViewModel: LocationViewModel?
    
    private weak var forecastViewModel: ForecastViewModel! {
        didSet {
            weatherIconImageView.image = forecastViewModel.icon
            temperatureLabel.text = forecastViewModel.temperature
            apparentTemperatureLabel.text = forecastViewModel.apparentTemperature
            windSpeedLabel.text = forecastViewModel.windSpeed
            humidityLabel.text = forecastViewModel.humidity
            precipProbabilityLabel.text = forecastViewModel.precipProbability
            
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.view.backgroundColor = self?.forecastViewModel.viewBackgroundColor
            }
        }
    }
    
    weak var delegate: MainFlowCoordinatorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationNameLabel.text = locationViewModel?.name
        requestForecastViewModel()
    }
    
    @IBAction func addLocationButtonAction(_ sender: Any) {
        delegate?.navigateToLocationViewController()
    }
    
    private func requestForecastViewModel() {
        ForecastViewModel.getForecastViewModel(locationViewModel: locationViewModel) { [weak self] (forecastViewModel, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let forecastViewModel = forecastViewModel {
                self?.forecastViewModel = forecastViewModel
            }
        }
    }
}
