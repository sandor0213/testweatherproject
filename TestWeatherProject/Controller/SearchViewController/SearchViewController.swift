//
//  SearchViewController.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

final class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchResultsTableView: UITableView!
    
    private var searchCompleter = MKLocalSearchCompleter()
    private var searchResults = [MKLocalSearchCompletion]()
    
    private var locationViewModels = [LocationViewModel]()
    private lazy var locationManager = makeLocationManager()
    private var didFindLocation = false
    
    weak var delegate: MainFlowCoordinatorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchCompleter.delegate = self
        searchResultsTableView.alwaysBounceVertical = false
        requestLocationViewModel()
    }
    
    @IBAction func getGeolocationButtonAction(_ sender: Any) {
        didFindLocation = false
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        delegate?.dismissSelf()
    }
    
    private func requestLocationViewModel() {
        LocationViewModel.getLocationViewModels { [weak self] (locationViewModels) in
            self?.locationViewModels = locationViewModels
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
    }
}

extension SearchViewController: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        searchResultsTableView.reloadData()
    }
}

extension SearchViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        cell.selectionStyle = .none
        return cell
    }
}

extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let completion = searchResults[indexPath.row]
        let searchRequest = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start {[weak self] (response, error) in
            guard let self = self else { return }
            let coordinate = response?.mapItems[0].placemark.coordinate
            let location = Location(name: response?.mapItems[0].placemark.title ?? "", longitude: Double(coordinate?.longitude ?? 0), lattitude: Double(coordinate?.latitude ?? 0), weather: nil)
            self.addLocation(locationViewModels: self.locationViewModels, location: location)
        }
    }
    
    func addLocation(locationViewModels: [LocationViewModel], location: Location, isCurrentLocation: Bool = false) {
        LocationManager.addLocation(locationViewModels: self.locationViewModels, location: location) { [weak self] (locationViewModels, alertMessage) in
            guard let self = self else { return }
            self.locationViewModels = locationViewModels
            UIAlertController.showAlert(in: self, message: alertMessage)
        }
    }
}

extension SearchViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !didFindLocation {
            didFindLocation = true
            locationManager.stopUpdatingLocation()
            let coordinate = locations.last?.coordinate
            LocationManager.getAddressFromCoordinate(coordinate: coordinate) { [weak self] (locationName, error) in
                guard let locationName = locationName else { return }
                guard let self = self else { return }
                let location = Location(name: locationName, longitude: Double(coordinate?.longitude ?? 0).format(), lattitude: Double(coordinate?.latitude ?? 0).format(), weather: nil)
                self.addLocation(locationViewModels: self.locationViewModels, location: location)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        if !LocationManager.isAuthorizatedLocation() {
            UIAlertController.showAlert(in: self, message: String.Alert.locationAuthorizatedStatusError)
        }
    }
    
    private func makeLocationManager() -> CLLocationManager {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        return manager
    }
}
