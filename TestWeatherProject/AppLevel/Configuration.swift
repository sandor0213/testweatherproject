//
//  Configuration.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

struct Configuration {
    static let secretKey = "25a891bd28ee4d4dfaec3ef76421619f"
    static let baseURL = "https://api.darksky.net/forecast/"
    static let baseURLWithSecretKey = baseURL + secretKey
}
