//
//  LocationProtocol.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 1/10/20.
//  Copyright © 2020 AdlerBalogh. All rights reserved.
//

protocol LocationProtocol {
    
    var name: String { get set }
    var longitude: Double { get set }
    var lattitude: Double { get set }
}
