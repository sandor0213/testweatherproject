//
//  WeatherProtocol.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 1/10/20.
//  Copyright © 2020 AdlerBalogh. All rights reserved.
//

protocol WeatherProtocol {

    var saveTime: Int { get }
    var icon: String { get set }
    var precipProbability: Double { get set }
    var temperature: Double { get set }
    var apparentTemperature: Double { get set }
    var humidity: Double { get set }
    var windSpeed: Double { get set }
}
