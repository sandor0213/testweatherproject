//
//  ProfileViewModel.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewModel {

    var name: String?
    var avatarImage: UIImage?

    init(profile: Profiles, completion: @escaping(ProfileViewModel) -> Void) {
        self.name = profile.name
        getImageFromUrlString(urlString: profile.avatar ?? "") { (avatar) in
            if let avatar = avatar {
                self.avatarImage = avatar
                completion(self)
            }
        }
    }
    
    static func getProfileViewModel(completion: @escaping(ProfileViewModel) -> Void) {
        let profile = StorageManager.shared.fetchData(entity: Profiles.self)
        if let profile = profile.last {
            ProfileViewModel(profile: profile) { (profileViewModel) in
                completion(profileViewModel)
            }
        }
    }
    
    private func getImageFromUrlString(urlString: String, completion: @escaping((UIImage?) -> Void)) {
        if let imageURL = URL(string: urlString) {
            KingfisherManager.shared.retrieveImage(with: imageURL, options: nil, progressBlock: nil, completionHandler: { result in
                switch result {
                case .success(let value):
                    completion(value.image)
                case .failure( _):
                    let image = UIImage.init(named: "avatarDefault")
                    completion(image)
                }
            })
        }
    }
}
