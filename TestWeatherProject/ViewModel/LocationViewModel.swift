//
//  LocationViewModel.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/11/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

struct LocationViewModel: Hashable {

    var name: String?
    var formattedCoordinates: String?
    var location: Locations?

    init <T: LocationProtocol>(location: T) {
        self.name = location.name
        self.formattedCoordinates = String(location.lattitude) + "," + String(location.longitude)
        if let location = location as? Locations {
            self.location = location
        }
    }
    
    static func getLocationViewModels(completion: @escaping([LocationViewModel]) -> Void) {
        var locationViewModels: [LocationViewModel] = []
        let locations = StorageManager.shared.fetchData(entity: Locations.self)
        for item in locations {
            let locationViewModel = LocationViewModel(location: item)
            locationViewModels.append(locationViewModel)
        }
        completion(locationViewModels)
    }
    
    static func deleteLocationViewModel<T: CVarArg>(entityKeyValue: T) {
        StorageManager.shared.deleteEntity(forEntityName: StorageConfiguration.locationsEntityName, key: "name", value: entityKeyValue)
    }

    static func == (lhs: LocationViewModel, rhs: LocationViewModel) -> Bool {
        return (lhs.name == rhs.name && lhs.formattedCoordinates == rhs.formattedCoordinates)
    }
}
