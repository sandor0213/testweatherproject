//
//  ForecastViewModel.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/10/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

class ForecastViewModel {

    var temperature: String?
    var apparentTemperature: String?
    var windSpeed: String?
    var precipProbability: String?
    var humidity: String?
    var icon: UIImage?
    var viewBackgroundColor: UIColor?

    init <T: WeatherProtocol>(weather: T) {
        self.temperature = String(Int(weather.temperature.fahrenheitToCelsius())) + String.Forecast.temperatureUnit
        self.apparentTemperature = String.Forecast.apparentTemperatureText + String(Int(weather.apparentTemperature.fahrenheitToCelsius())) + String.Forecast.temperatureUnit
        self.windSpeed = String.Forecast.windSpeedText + String(Int(weather.windSpeed.mphToKmh())) + String.Forecast.windSpeedUnit
        self.precipProbability = String.Forecast.precipProbabilityText + String(Int(weather.precipProbability))
        self.humidity = String.Forecast.humidityText + String(Int(weather.humidity * 100)) + String.Forecast.percent
        if let icon = UIImage(named: weather.icon) {
            self.icon = icon
        }
        self.viewBackgroundColor = UIColor.colors[weather.icon]
    }

    static func getForecastViewModel(locationViewModel: LocationViewModel?, completion: @escaping(ForecastViewModel?, Error?) -> Void) {
        let location = StorageManager.shared.fetchEntity(entity: Locations.self, key: "name", value: locationViewModel?.name ?? "")
        if let time = location?.weather?.saveTime {
            guard time + Constants.updateInterval < Int(Date().timeIntervalSince1970) else {
                let forecastViewModel: ForecastViewModel? = location?.weather.map({ return ForecastViewModel(weather: $0)})
                completion(forecastViewModel, nil)
                return
            }
        }
        
        guard let locationViewModel = locationViewModel else {
            completion(nil, Error?.self as? Error)
            return
        }
        
        fetchForecast(locationViewModel: locationViewModel) { (forecast, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(nil, error)
                return
            }
            
            if let weather = forecast?.currently {
                let forecastViewModel = ForecastViewModel(weather: weather)
                completion(forecastViewModel, nil)
            }
        }
    }
    
    static func fetchForecast(locationViewModel: LocationViewModel, completion: @escaping(Forecast?, Error?) -> Void) {
        Service.sharedInstance.fetchForecast(coordinations: locationViewModel.formattedCoordinates ?? "") { (forecast, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(nil, error)
                return
            }
            guard let forecast = forecast, let locations = locationViewModel.location else {
                completion(nil, error)
                return
            }
            
            let weather = StorageManager.shared.createData(entity: Weathers.self, model: forecast.currently, forEntityName: StorageConfiguration.weathersEntityName)
            locations.weather = weather.first
            StorageManager.shared.updateEntity(entity: locations)
            completion(forecast, error)
        }
    }
}

extension ForecastViewModel {
    struct Constants {
        static let updateInterval = 900
    }
}
