//
//  Encodable+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/11/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import Foundation

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed)).flatMap { $0 as? [String: Any] }
    }
}
