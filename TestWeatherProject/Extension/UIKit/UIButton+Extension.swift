//
//  UIButton+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setPositiveButton() {
        layer.backgroundColor = UIColor.positiveButtonColor.cgColor
        layer.borderWidth = 0
    }
    
    func setNegativeButton() {
        backgroundColor = .clear
        layer.borderWidth = 1
        layer.borderColor = UIColor.negativeButtonColor.cgColor
        layer.cornerRadius = layer.frame.height / 2
        layer.masksToBounds = true
    }
}
