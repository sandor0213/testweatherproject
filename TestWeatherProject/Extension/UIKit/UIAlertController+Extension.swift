//
//  UIAlertController+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/12/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    static func showAlert(in viewController: UIViewController, title: String? = nil, message: String? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: String.Alert.alertConfirmButtonTitle, style: .default)
        alertController.addAction(confirmAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
