//
//  UIView+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

extension UIView {
    
    func setCircle() {
        layer.cornerRadius = frame.height / 2
        layer.masksToBounds = true
    }
}
