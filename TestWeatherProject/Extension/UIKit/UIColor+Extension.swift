//
//  UIColor+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var positiveButtonColor: UIColor {
        return UIColor(hexString: "#FF1F56")
    }
    
    class var negativeButtonColor: UIColor {
        return UIColor(hexString: "#0055FF")
    }
    
    static let colors:[String:UIColor] =  [
        "clear-day"           : UIColor(hexString: "#2196f3"),
        "clear-night"         : UIColor(hexString: "#311b92"),
        "rain"                : UIColor(hexString: "#9e9e9e"),
        "snow"                : UIColor(hexString: "#607d8b"),
        "sleet"               : UIColor(hexString: "#90a4ae"),
        "wind"                : UIColor(hexString: "#26a69a"),
        "fog"                 : UIColor(hexString: "#b0bec5"),
        "cloudy"              : UIColor(hexString: "#b0bec5"),
        "partly-cloudy-day"   : UIColor(hexString: "#26c6da"),
        "partly-cloudy-night" : UIColor(hexString: "#1a237e")
    ]
    
    convenience init(hexString: String, alpha: CGFloat = 1) {
        var hex: UInt32 = 0
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        scanner.scanHexInt32(&hex)
        self.init(hex: hex)
    }
    
    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        self.init(red: CGFloat((hex >> 16) & 0xFF) / 255,
                  green: CGFloat((hex >> 8) & 0xFF) / 255,
                  blue: CGFloat(hex & 0xFF) / 255,
                  alpha: alpha)
    }
}
