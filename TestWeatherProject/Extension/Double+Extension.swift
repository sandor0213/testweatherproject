//
//  Double+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/10/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

extension Double {
    
    func fahrenheitToCelsius() -> Double {
        let celsius = (self - 32.0) * (5.0/9.0)
        return celsius
    }
    
    func mphToKmh() -> Double {
        return self * 1.60934
    }
    
    func format(f: String = ".2") -> Double {
        if let shortDouble = Double(String(format: "%\(f)f", self)) {
            return shortDouble
        }
        return self
    }
}
