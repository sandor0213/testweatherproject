//
//  String+Extension.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/12/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

extension String {
    
    struct Alert {
        static let addSuccessMessage = "This location is successfully added to your list"
        static let addCurrentSuccessMessage = "Your current location is successfully added to your list"
        static let addErrorMessage = "This location is already added to your list"
        static let alertConfirmButtonTitle = "Ok"
        static let locationAuthorizatedStatusError = "Please allow location access in your settings"
    }
    
    struct Forecast {
        static let apparentTemperatureText = "Feels like: "
        static let windSpeedText = "Wind: "
        static let humidityText = "Humidity: "
        static let precipProbabilityText = "Chance of rain: "
        static let temperatureUnit = " ºC"
        static let windSpeedUnit = " km/h"
        static let percent = "%"
    }
}
