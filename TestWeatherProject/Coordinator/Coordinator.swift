//
//  Coordinator.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

public protocol Coordinator: class {
    
    var childCoordinators: [Coordinator] {get set}
    
    init(navigationController: UINavigationController)
    
    func start()
}
