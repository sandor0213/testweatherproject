//
//  MainCoordinator.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/12/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

final class MainCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.navigationController.isNavigationBarHidden = true
    }
    
    func start() {
        let profile = StorageManager.shared.fetchData(entity: Profiles.self)
        if profile.last != nil {
            let mainFlowCoordinator: MainFlowCoordinator = MainFlowCoordinator(navigationController: navigationController)
            childCoordinators.append(mainFlowCoordinator)
            mainFlowCoordinator.start()
        } else {
            let authFlowCoordinator: AuthFlowCoordinator = AuthFlowCoordinator(navigationController: navigationController)
            childCoordinators.append(authFlowCoordinator)
            authFlowCoordinator.start()
        }
    }
}
