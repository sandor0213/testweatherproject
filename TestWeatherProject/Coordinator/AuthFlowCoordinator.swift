//
//  LogInCoordinator.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit

final class AuthFlowCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let logInViewController: LogInViewController = LogInViewController()
        logInViewController.delegate = self
        navigationController.pushViewController(logInViewController, animated: true)
    }
}

extension AuthFlowCoordinator: AuthFlowCoordinatorDelegate {
    
    func navigateToMainFlow() {
        let mainFlowCoordinator: MainFlowCoordinator = MainFlowCoordinator(navigationController: navigationController)
        childCoordinators.removeAll()
        childCoordinators.append(mainFlowCoordinator)
        mainFlowCoordinator.start()
    }
}
