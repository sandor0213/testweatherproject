//
//  AuthFlowCoordinatorDelegate.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 1/10/20.
//  Copyright © 2020 AdlerBalogh. All rights reserved.
//

public protocol AuthFlowCoordinatorDelegate: class {
    func navigateToMainFlow()
}
