//
//  MainFlowCoordinatorDelegate.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 1/10/20.
//  Copyright © 2020 AdlerBalogh. All rights reserved.
//

protocol MainFlowCoordinatorDelegate: class {
    func navigateToSearchViewController()
    func dismissSelf()
    func navigateToForecastViewController(locationViewModel: LocationViewModel)
    func navigateToLocationViewController()
    func navigateToProfileViewController()
    func navigateToAuthFlow()
}
