//
//  MainFlowCoordinator.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 1/10/20.
//  Copyright © 2020 AdlerBalogh. All rights reserved.
//

import UIKit

final class MainFlowCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []

    unowned let navigationController: UINavigationController

    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let locationViewController: LocationViewController = LocationViewController()
        locationViewController.delegate = self
        navigationController.pushViewController(locationViewController, animated: true)
    }
}

extension MainFlowCoordinator : MainFlowCoordinatorDelegate {

    func navigateToSearchViewController() {
        let searchViewController: SearchViewController = SearchViewController()
        searchViewController.delegate = self
        searchViewController.modalPresentationStyle = .fullScreen
        navigationController.present(searchViewController, animated: true)
    }

    func navigateToForecastViewController(locationViewModel: LocationViewModel) {
        let forecastViewController : ForecastViewController = ForecastViewController()
        forecastViewController.delegate = self
        forecastViewController.locationViewModel = locationViewModel
        navigationController.pushViewController(forecastViewController, animated: true)
    }

    func dismissSelf() {
        navigationController.dismiss(animated: true)
    }

    func navigateToLocationViewController() {
        let locationViewController: LocationViewController = LocationViewController()
        locationViewController.delegate = self
        navigationController.pushViewController(locationViewController, animated: true)
    }

    func navigateToProfileViewController() {
        let profileViewController: ProfileViewController = ProfileViewController()
        profileViewController.delegateMain = self
        navigationController.pushViewController(profileViewController, animated: true)
    }

    func navigateToAuthFlow() {
        let authFlowCoordinator = AuthFlowCoordinator(navigationController: navigationController)
        childCoordinators.removeAll()
        childCoordinators.append(authFlowCoordinator)
        authFlowCoordinator.start()
    }
}
