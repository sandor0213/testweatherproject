//
//  AuthenticationManager.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/10/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import FBSDKLoginKit

final class AuthenticationManager {
    
    static func loginViaFacebook(controller: UIViewController, with completion: @escaping((Error?) -> Void)) {
        LoginManager().logIn(permissions: ["email"], from: controller) { (loginResult, error) in
            guard error == nil else {
                completion(error)
                return
            }
            
            guard let fbloginresult = loginResult, !fbloginresult.isCancelled, fbloginresult.grantedPermissions.contains("email"),
                AccessToken.current != nil else { return }
            
            let request = GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large)"])
            request.start { (connection, result, error) in
                guard error == nil else {
                    completion(error)
                    return
                }
                
                guard let userInfo = result as? [String: Any] else {
                    completion(error)
                    return
                }
                
                if let name = userInfo["name"] as? String, let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                    let profile = Profile(name: name, avatar: imageURL)
                    StorageManager.shared.createData(model: profile, forEntityName: StorageConfiguration.profilesEntityName)
                    completion(nil)
                }
            }
        }
    }
    
    static func logOutFacebook() {
        let loginManager = LoginManager()
        loginManager.logOut()
    }
}
