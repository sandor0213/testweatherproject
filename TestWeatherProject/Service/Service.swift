//
//  Service.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import Foundation

struct Service {
    
    static let sharedInstance = Service()
    
    private func getRequest<T: Decodable>(coordinations: String, completion: @escaping (T?, Error?) -> Void) {
        let urlString = Configuration.baseURLWithSecretKey + "/" + coordinations
        guard let url = URL(string: urlString) else { return }
        
        DispatchQueue.global(qos: .background).async {
            URLSession.shared.dataTask(with: url) { (data, resp, error) in
                if let error = error {
                    completion(nil, error)
                    print("Failed:", error)
                    return
                }
                
                guard let data = data else { return }
                
                do {
                    let decodedResponse = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        completion(decodedResponse, nil)
                    }
                } catch let JsonError as NSError {
                    print(JsonError)
                }
            }.resume()
        }
    }
    
    func fetchForecast(coordinations: String, completion: @escaping (Forecast?, Error?) -> Void) {
        let requestCompletion: (Forecast?, Error?) -> Void = { (forecast, error) in
            completion(forecast, error)
        }
        
        getRequest(coordinations: coordinations, completion: requestCompletion)
    }
}
