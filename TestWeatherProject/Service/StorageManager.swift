//
//  StorageManager.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/10/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import UIKit
import CoreData

final class StorageManager {
    
    static let shared = StorageManager()
    
    func createData<T: Encodable, Z: NSManagedObject>(entity: Z.Type? = nil, model: T, forEntityName: String) -> [Z] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: forEntityName, in: managedContext) else { return [] }
        if let dictionary = model.dictionary {
            let entityObject = NSManagedObject(entity: entity, insertInto: managedContext)
            entityObject.setValuesForKeys(dictionary)
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
            if let entityObjects = [entityObject] as? [Z] {
                return entityObjects
            }
        }
        return []
    }
    
    func fetchData<T:NSManagedObject>(entity: T.Type) -> [T] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        
        let fetchRequest = T.fetchRequest()
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            let result = try managedContext.fetch(fetchRequest)
            if let entities: [T] = result as? [T] {
                return entities
            }
        } catch {
            return []
        }
        return []
    }
    
    func fetchEntity<T: NSManagedObject, U: CVarArg>(entity: T.Type, key: String, value: U) -> T? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = T.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "\(key) == %@", value)
        do {
            let result = try managedContext.fetch(fetchRequest)
            let entity = result.first as? T
            return entity
        } catch {
            return nil
        }
    }
    
    func updateEntity<T:NSManagedObject>(entity: T) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.refresh(entity, mergeChanges: true)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not update. \(error), \(error.userInfo)")
        }
    }
    
    func deleteData(forEntityName: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: forEntityName)
        do {
            let result = try managedContext.fetch(fetchRequest)
            if let objectsToDelete = result as? [NSManagedObject] {
                for objectToDelete in objectsToDelete {
                    managedContext.delete(objectToDelete)
                }
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                }
            }
        } catch let error as NSError {
            print("Could not fetch for deleting. \(error), \(error.userInfo)")
        }
    }
    
    func deleteEntity<T: CVarArg>(forEntityName: String, key: String, value: T) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: forEntityName)
        fetchRequest.predicate = NSPredicate(format: "\(key) == %@", value)
        do {
            let entities = try managedContext.fetch(fetchRequest)
            if let objectToDelete = entities.first as? NSManagedObject {
                managedContext.delete(objectToDelete)
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("Could not delete. \(error), \(error.userInfo)")
                }
            }
        } catch let error as NSError {
            print("Could not fetch for deleting. \(error), \(error.userInfo)")
        }
    }
    
    func clearLocalDatabase(forEntitiesName: [String]) {
        for item in forEntitiesName {
            deleteData(forEntityName: item)
        }
    }
}
