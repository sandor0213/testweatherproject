//
//  LocationManager.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/13/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import CoreLocation

class LocationManager {
    
    static func getAddressFromCoordinate(coordinate: CLLocationCoordinate2D?, completion: @escaping (String?, Error?) -> Void) {
        if let coordinate = coordinate {
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                guard error == nil else {
                    completion(nil, error)
                    return
                }
                
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                var locationName = ""
                if let locality = placeMark.locality {
                    locationName.append(contentsOf: locality)
                }
                if let administrativeArea = placeMark.administrativeArea, administrativeArea != (placeMark.locality ?? "") {
                    locationName.append(contentsOf: locationName.isEmpty ? administrativeArea : (", " + administrativeArea))
                }
                if let country = placeMark.country {
                    locationName.append(contentsOf: locationName.isEmpty ? country: (", " + country))
                }
                completion(locationName, error)
            })
        }
    }
    
    static func isAuthorizatedLocation() -> Bool {
        return CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
    }
    
    static func addLocation(locationViewModels: [LocationViewModel], location: Location, isCurrentLocation: Bool = false, completion: @escaping([LocationViewModel], String) -> Void) {
        var locationViewModelsSet = Set(locationViewModels)
        let locationViewModel = LocationViewModel(location: location)
            locationViewModelsSet.insert(locationViewModel)
        let updatedLocationViewModels = Array(locationViewModelsSet)
        if updatedLocationViewModels.count > locationViewModels.count {
            completion(updatedLocationViewModels, isCurrentLocation ? String.Alert.addCurrentSuccessMessage : String.Alert.addSuccessMessage)
            StorageManager.shared.createData(model: location, forEntityName: StorageConfiguration.locationsEntityName)
        } else {
            completion(locationViewModels, String.Alert.addErrorMessage)
        }
    }
}
