//
//  Profile.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

struct Profile: Encodable {
    
    let name: String
    let avatar: String
}
