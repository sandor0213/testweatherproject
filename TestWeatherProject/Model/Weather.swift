//
//  Weather.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/9/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

import Foundation

struct Weather: Codable, WeatherProtocol {
    
    let saveTime = Int(Date().timeIntervalSince1970)
    var icon: String
    var precipProbability: Double
    var temperature: Double
    var apparentTemperature: Double
    var humidity: Double
    var windSpeed: Double
}
