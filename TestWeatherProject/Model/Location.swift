//
//  Location.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/11/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

struct Location: Encodable, LocationProtocol {

    var name: String
    var longitude: Double
    var lattitude: Double
    var weather: Weather?
}
