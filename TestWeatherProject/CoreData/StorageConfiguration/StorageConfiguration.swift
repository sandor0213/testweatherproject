//
//  StorageConfiguration.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/12/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//

struct StorageConfiguration {
    
    static let profilesEntityName = "Profiles"
    static let locationsEntityName = "Locations"
    static let weathersEntityName = "Weathers"
}
