//
//  Locations+CoreDataProperties.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/15/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//
//

import Foundation
import CoreData


extension Locations: LocationProtocol {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Locations> {
        return NSFetchRequest<Locations>(entityName: StorageConfiguration.locationsEntityName)
    }
    
    @NSManaged public var lattitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String
    @NSManaged public var weather: Weathers?
}
