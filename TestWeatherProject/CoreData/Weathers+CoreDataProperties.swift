//
//  Weathers+CoreDataProperties.swift
//  TestWeatherProject
//
//  Created by Balogh Sandor on 12/15/19.
//  Copyright © 2019 AdlerBalogh. All rights reserved.
//
//

import Foundation
import CoreData


extension Weathers: WeatherProtocol {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weathers> {
        return NSFetchRequest<Weathers>(entityName: StorageConfiguration.weathersEntityName)
    }
    
    @NSManaged public var apparentTemperature: Double
    @NSManaged public var humidity: Double
    @NSManaged public var icon: String
    @NSManaged public var precipProbability: Double
    @NSManaged public var temperature: Double
    @NSManaged public var saveTime: Int
    @NSManaged public var windSpeed: Double
    @NSManaged public var ofLocation: Locations?
}
